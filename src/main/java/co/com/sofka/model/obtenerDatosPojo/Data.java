package co.com.sofka.model.obtenerDatosPojo;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Data{
	private String lastName;
	private Integer id;
	private String avatar;
	private String firstName;
	private String email;

	@JsonProperty("last_name")
	public void setLastName(String lastName){
		this.lastName = lastName;
	}

	@JsonProperty("last_name")
	public String getLastName(){
		return lastName;
	}

	@JsonProperty("first_name")
	public void setFirstName(String firstName){
		this.firstName = firstName;
	}

	@JsonProperty("first_name")
	public String getFirstName(){
		return firstName;
	}


	public void setEmail(String email){
		this.email = email;
	}

	public String getEmail(){
		return email;
	}

	public void setId(Integer id){
		this.id = id;
	}

	public Integer getId(){
		return id;
	}

	public void setAvatar(String avatar){
		this.avatar = avatar;
	}

	public String getAvatar(){
		return avatar;
	}

}
